
package prueba3;

public class NodoEnvio {
    private Envio dato;
    private NodoEnvio next;

    public NodoEnvio(Envio dato, NodoEnvio next) {
        this.dato = dato;
        this.next = next;
    }

    public Envio getDato() {
        return dato;
    }

    public NodoEnvio getNext() {
        return next;
    }

    public void setNext(NodoEnvio next) {
        this.next = next;
    }
}
