
package prueba3;

public class Empresa {
    private String representateLegal;
    private String fechaCreacion;
    private Cliente[] clientes;
    private int pLibre;

    public Empresa(String representateLegal, String fechaCreacion, int maxClientes) {
        this.representateLegal = representateLegal;
        this.fechaCreacion = fechaCreacion;
        this.clientes = new Cliente[maxClientes];
    }
    
    public boolean agregarEnvio(int idCliente, Envio envio) {
        int sumaDespachos = 0;
        int posCliente = -1;
        
        for(int i = 0; i < pLibre; i++) {
            sumaDespachos += clientes[i].sumarDiasDespachosActivos();
            
            if(clientes[i].getId() == idCliente)
                posCliente = i;
        }
        
        if(sumaDespachos > 1000)
            return false;
        
        if(posCliente == -1)
            return false;
        
        clientes[posCliente].agregarEnvioAlFinal(envio);
        return true;
    }
    
    public Envio eliminarOfertaVIP(String codOferta) {
        for(int i = 0; i < pLibre; i++) {
            Envio eliminado = clientes[i].eliminarOfertaVIP(codOferta);
            
            if(eliminado != null)
                return eliminado;
        }
        
        return null;
    }
}
