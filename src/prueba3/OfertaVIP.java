
package prueba3;

public class OfertaVIP  extends Envio {
    private String codOferta;

    public OfertaVIP(String codOferta, String id, String direccion, int costoDesp, boolean activo) {
        super(id, direccion, costoDesp, activo);
        this.codOferta = codOferta;
    }
    
    @Override
    public int calcularDiasDespachoEstimados(int prioridad) {
        return (int)(getCostoDespacho() / (double)(prioridad * 10));
    }

    public String getCodigoOferta() {
        return codOferta;
    }
}
