
package prueba3;

public class PaqueteEstandar extends Envio {
    private int maxDiasProc;

    public PaqueteEstandar(int maxDiasProc, String id, String direccion, int costoDesp, boolean activo) {
        super(id, direccion, costoDesp, activo);
        this.maxDiasProc = maxDiasProc;
    }
    
    @Override
    public int calcularDiasDespachoEstimados(int prioridad) {
        if(!isActivo())
            return 0;
        
        if(prioridad > 5)
            return maxDiasProc * prioridad;
        else
            return (int)(maxDiasProc / (double)prioridad);
    }
    
}
