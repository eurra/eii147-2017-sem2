
package prueba3;

public abstract class Envio {
    private String id;
    private String direccion;
    private int costoDesp;
    private boolean activo;

    public Envio(String id, String direccion, int costoDesp, boolean activo) {
        this.id = id;
        this.direccion = direccion;
        this.costoDesp = costoDesp;
        this.activo = activo;
    }

    public String getId() {
        return id;
    }

    public String getDireccion() {
        return direccion;
    }

    public int getCostoDespacho() {
        return costoDesp;
    }

    public boolean isActivo() {
        return activo;
    }
    
    public abstract int calcularDiasDespachoEstimados(int prioridad);
}
