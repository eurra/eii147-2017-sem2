
package prueba3;

public class Cliente {
    private int id;
    private String nombre;
    private NodoEnvio head;

    public Cliente(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
        this.head = new NodoEnvio(null, null);
    }

    public int getId() {
        return id;
    }
    
    public void agregarEnvioAlFinal(Envio envio) {
        NodoEnvio aux = head;
        
        while(aux.getNext() != null)
            aux = aux.getNext();
        
        aux.setNext(new NodoEnvio(envio, null));
    }
    
    public int sumarDiasDespachosActivos() {
        int suma = 0;
        NodoEnvio aux = head.getNext();
        
        while(aux != null) {
            if(aux.getDato().isActivo()) {
                suma += aux.getDato().calcularDiasDespachoEstimados(10);
            }
            
            aux = aux.getNext();
        }
        
        return suma;
    }
    
    public Envio eliminarOfertaVIP(String codOferta) {
        NodoEnvio aux = head;
        
        while(aux.getNext() != null) {
            if(aux.getNext().getDato() instanceof OfertaVIP) {
                OfertaVIP oVIP = (OfertaVIP)aux.getNext().getDato();
                
                if(oVIP.getCodigoOferta().equals(codOferta)) {
                    aux.setNext(aux.getNext().getNext());
                    return oVIP;
                }
            }
            
            aux = aux.getNext();
        }
        
        return null;
    }
}
