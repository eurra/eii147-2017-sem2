
package sem16;

public class Servel {
    private String director;
    private Mesa[] mesas;
    private int pLibre;

    public Servel(String director, int cantMesas) {
        this.director = director;
        mesas = new Mesa[cantMesas];
    }  
    
    public boolean agregarVoto(Voto voto, int idMesa) {
        Mesa mesa = buscarMesa(idMesa);
        
        if(mesa == null)
            return false;
        
        mesa.agregarVoto(voto);
        return true;
    }
    
    public boolean fusionarMesas(int mesa1, int mesa2) {
        int indiceMesa1 = buscarIndiceMesa(mesa1);
        
        if(indiceMesa1 == -1)
            return false;
        
        int indiceMesa2 = buscarIndiceMesa(mesa2);
        
        if(indiceMesa2 == -1)
            return false;
        
        int cuentaMesa1 = mesas[indiceMesa1].contarVotos();
        int cuentaMesa2 = mesas[indiceMesa2].contarVotos();
        int indiceMesaMuere;
        Mesa mesaQueda, mesaMuere;
        
        if(cuentaMesa1 > cuentaMesa2) {
            mesaQueda = mesas[indiceMesa1];
            mesaMuere = mesas[indiceMesa2];
            indiceMesaMuere = indiceMesa2;
        }
        else {
            mesaQueda = mesas[indiceMesa2];
            mesaMuere = mesas[indiceMesa1];
            indiceMesaMuere = indiceMesa1;
        }
        
        mesaQueda.traspasarVotosDesde(mesaMuere);
        mesas[indiceMesaMuere] = mesas[pLibre - 1];
        pLibre--;
        
        return true;
    }
    
    private Mesa buscarMesa(int id) {
        int indice = buscarIndiceMesa(id);
        
        if(indice == -1)        
            return null;
        
        return mesas[indice];
    }
    
    private int buscarIndiceMesa(int id) {
        for(int i = 0; i < pLibre; i++) {
            if(mesas[i].getId() == id)
                return i;
        }
        
        return -1;
    }
    
    public String obtenerGanadorPresidencial() {
        // Paso 1: contar cantidad máxima de posibles candidatos = total de votos presidenciales
        int totalVotosPres = 0;
        
        for(int i = 0; i < pLibre; i++)
            totalVotosPres += mesas[i].contarVotosPresidenciales();
        
        if(totalVotosPres == 0) // No hay candidatos presidenciales
            return null;
        
        // Paso 2: llenar un arreglo compacto con los nombres de los candidatos presidenciales (no repetidos)
        String[] nombresCandPres = new String[totalVotosPres];
        
        for(int i = 0; i < pLibre; i++)
            mesas[i].guardarCandidatosPresindencialesNoRepetidos(nombresCandPres);
        
        // Paso 3: para cada nombre de candidato, contar su cantidad de votos en todo el sistema
        int[] votosCandPres = new int[totalVotosPres];
        
        for(int i = 0; i < nombresCandPres.length && nombresCandPres[i] != null; i++) {
            int votosCantActual = 0;
            
            for(int j = 0; i < pLibre; i++)
                votosCantActual += mesas[j].contarVotosPresidenciales(nombresCandPres[i]);
            
            votosCandPres[i] = votosCantActual;
        }
        
        // Paso 4: Determinar el candidato con mayor cantidad de votos
        String candMayor = null;
        int cuentaMayor = 0;
        
        for(int i = 0; i < nombresCandPres.length && nombresCandPres[i] != null; i++) {
            if(candMayor == null || cuentaMayor < votosCandPres[i]) {
                candMayor = nombresCandPres[i];
                cuentaMayor = votosCandPres[i];
            }
        }
        
        // Finalmente, retornar resultado
        return candMayor;
    }
    
    public boolean agregarMesa(Mesa m) {
        if(pLibre == mesas.length)
            return false;
        
        mesas[pLibre] = m;
        pLibre++;
        return true;
    }
    
    public String[] obtenerInfoMesas() {
        String[] info = new String[pLibre];
        
        for(int i = 0; i < pLibre; i++) {
            info[i] = "Mesa id: " + mesas[i].getId() + 
                        ", cant. votos: " + mesas[i].contarVotos();
        }
        
        return info;
    }
}
