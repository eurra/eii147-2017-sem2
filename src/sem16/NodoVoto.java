
package sem16;

public class NodoVoto {
    private Voto dato;
    private NodoVoto next;

    public NodoVoto(Voto dato, NodoVoto next) {
        this.dato = dato;
        this.next = next;
    }

    public Voto getDato() {
        return dato;
    }

    public NodoVoto getNext() {
        return next;
    }

    public void setNext(NodoVoto next) {
        this.next = next;
    }
}
