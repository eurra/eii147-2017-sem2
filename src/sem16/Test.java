
package sem16;

public class Test {    
    public static void main(String[] args) {
        Servel servel = new Servel("Director", 5);
        
        servel.agregarMesa(new Mesa(1, "region 1"));
        servel.agregarMesa(new Mesa(2, "region 2"));
        servel.agregarMesa(new Mesa(3, "region 3"));
        
        imprimirInfoMesas(servel.obtenerInfoMesas());
        
        servel.agregarVoto(new VotoPresidencial(1, "cand 1"), 1);
        servel.agregarVoto(new VotoPresidencial(2, "cand 2"), 1);
        servel.agregarVoto(crearVotoPrueba(3, "test"), 1);
        
        servel.agregarVoto(new VotoPresidencial(4, "cand 1"), 2);
        servel.agregarVoto(crearVotoPrueba(5, "test"), 2);
        
        servel.agregarVoto(new VotoPresidencial(6, "cand 1"), 3);
        servel.agregarVoto(new VotoPresidencial(7, "cand 2"), 3);
        servel.agregarVoto(crearVotoPrueba(8, "test"), 3);
        servel.agregarVoto(crearVotoPrueba(9, "test"), 3);
        
        servel.agregarVoto(new VotoPresidencial(10, "cand 1"), 4); // false
        
        imprimirInfoMesas(servel.obtenerInfoMesas());
        System.out.println("Candidato presidencial ganador: " + servel.obtenerGanadorPresidencial() + "\n"); // cand 1
        
        servel.fusionarMesas(1, 2);
        imprimirInfoMesas(servel.obtenerInfoMesas());
        
        System.out.println("Candidato presidencial ganador: " + servel.obtenerGanadorPresidencial() + "\n"); // cand 1
    }
    
    private static void imprimirInfoMesas(String[] info) {
        for(int i = 0; i < info.length; i++)
            System.out.println(info[i]);
        
        System.out.println();
    }
    
    private static Voto crearVotoPrueba(int num, String cand) {
        return new Voto(num, cand) {
            @Override
            public int calcularMontoDevolucion(double factor) {
                return 0;
            }
        };
    }
}
