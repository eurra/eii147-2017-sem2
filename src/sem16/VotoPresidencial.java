
package sem16;

/**
 *
 * @author Enrique
 */
public class VotoPresidencial extends Voto {

    public VotoPresidencial(int num, String candidato) {
        super(num, candidato);
    }
    
    @Override
    public int calcularMontoDevolucion(double factor) {
        return 0; // ajustar al enunciado
    }
    
}
