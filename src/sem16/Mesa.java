
package sem16;

public class Mesa {
    private NodoVoto head;
    private int id;
    private String region;

    public Mesa(int id, String region) {
        this.head = null; // Sin nodo fantasma, línea opcional
        // this.head = new NodoVoto(null, null); // Con nodo fantasma, línea obligatoria
        this.id = id;
        this.region = region;
    }

    public int getId() {
        return id;
    }
    
    public void agregarVoto(Voto v) {
        NodoVoto nuevo = new NodoVoto(v, null);
                
        if(head == null) {
            head = nuevo;
        }
        else {
            NodoVoto aux = head;
            
            while(aux.getNext() != null)
                aux = aux.getNext();
            
            aux.setNext(nuevo);
        }
    }
    
    public int contarVotosPresidenciales(String cand) {
        NodoVoto aux = head;
        int cant = 0;
        
        while(aux != null) {
            if(aux.getDato() instanceof VotoPresidencial) {
                if(cand == null || aux.getDato().getCandidato().equals(cand))
                    cant++;
            }
            
            aux = aux.getNext();
        }
        
        return cant;
    }
    
    public int contarVotosPresidenciales() {
        return contarVotosPresidenciales(null);
    }

    public void guardarCandidatosPresindencialesNoRepetidos(String[] nombresCandPres) {
        NodoVoto aux = head;
        
        while(aux != null) {
            if(aux.getDato() instanceof VotoPresidencial) {
                agregarCandidatoNoRepetido(nombresCandPres, aux.getDato().getCandidato());
            }
            
            aux = aux.getNext();
        }
    }
    
    private void agregarCandidatoNoRepetido(String[] nombres, String agregar) {
        int i;
        
        for(i = 0; i < nombres.length && nombres[i] != null; i++) {
            if(nombres[i].equals(agregar))
                return;
        }
        
        nombres[i] = agregar;
    }
    
    public int contarVotos() {
        NodoVoto aux = head;
        int cuenta = 0;
        
        while(aux != null) {
            cuenta++;
            aux = aux.getNext();
        }
        
        return cuenta;
    }
    
    public void traspasarVotosDesde(Mesa otraMesa) {
        if(head == null) {
            head = otraMesa.head;
        }
        else {
            NodoVoto aux = head;
            
            while(aux.getNext() != null)
                aux = aux.getNext();
            
            aux.setNext(otraMesa.head);
        }
    }
}
