
package sem16;

public abstract class Voto {
    private int num;
    private String candidato;

    public Voto(int num, String candidato) {
        this.num = num;
        this.candidato = candidato;
    }

    public String getCandidato() {
        return candidato;
    }
    
    public abstract int calcularMontoDevolucion(double factor);
}
