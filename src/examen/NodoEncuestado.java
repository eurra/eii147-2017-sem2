
package examen;

public class NodoEncuestado {
    private Encuestado encuestado;
    private NodoEncuestado sig;
    
    public NodoEncuestado(Encuestado encuestado, NodoEncuestado sig) { 
        this.encuestado = encuestado;
        this.sig = sig;
    }

    public NodoEncuestado getSig() {
        return sig;
    }

    public Encuestado getEncuestado() {
        return encuestado;
    }

    public void setSig(NodoEncuestado sig) {
        this.sig = sig;
    }
}
