
package examen;

public class EncuestadoIndeciso extends Encuestado {
    public EncuestadoIndeciso(String nombre, String rut, String seleccion) { 
        super(nombre, rut, seleccion);
    }
    
    @Override
    public double calcularMargenError(int votos) {
        return Math.sqrt(votos)*0.25; 
    }
}
