
package examen;

public class EncuestadoPartidario extends Encuestado {
    private String nombrePartido;
    
    public EncuestadoPartidario(String nombrePartido, String nombre, String rut, String seleccion) { 
        super(nombre, rut, seleccion); 
        this.nombrePartido = nombrePartido;
    }
    
    @Override
    public double calcularMargenError(int votos) {
        return votos * 0.084; 
    }
}
