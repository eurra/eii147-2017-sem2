
package examen;

public class Encuesta {
    private int numero;
    private String fecha;
    private int cantEncuestados;
    private NodoEncuestado head;
    
    public Encuesta(int numero, String fecha) { 
        this.numero = numero;
        this.fecha = fecha;
        this.cantEncuestados = 0;
        this.head = new NodoEncuestado(null, null);
    }

    public int getNumero() {
        return numero;
    }
    
    public boolean agregarEncuestado(Encuestado enc, int votos) {
        if(existeEncuestado(enc))
            return false;
        
        double sumaMargenErr = calcularSumaMargenError(votos);
        
        if(sumaMargenErr > 100.0)
            return false;
        
        NodoEncuestado aux = head;
        
        while(aux.getSig() != null)
            aux = aux.getSig();
        
        aux.setSig(new NodoEncuestado(enc, null));
        cantEncuestados++;
        return true;
    }
    
    private boolean existeEncuestado(Encuestado enc) {
        NodoEncuestado aux = head.getSig();
        
        while(aux != null) {
            if(aux.getEncuestado().getRut().equals(enc.getRut()))
                return true;
            
            aux = aux.getSig();
        }
        
        return false;
    }
    
    private double calcularSumaMargenError(int votos) {
        double total = 0;
        NodoEncuestado aux = head.getSig();
        
        while(aux != null) {
            total += aux.getEncuestado().calcularMargenError(votos);
            aux = aux.getSig();
        }

        return total;
    }

    public Encuestado[] obtenerEncuestadosOrdenados() {
        Encuestado[] res = new Encuestado[cantEncuestados];
        NodoEncuestado aux = head.getSig();
        int pos = 0;
        
        while(aux != null) {
            if(aux.getEncuestado() instanceof EncuestadoPartidario) {
                res[pos] = aux.getEncuestado();
                pos++;
            }
            
            aux = aux.getSig();
        }
        
        aux = head.getSig();
        
        while(aux != null) {
            if(aux.getEncuestado() instanceof EncuestadoIndeciso) {
                res[pos] = aux.getEncuestado();
                pos++;
            }
            
            aux = aux.getSig();
        }
        
        return res;
    }
}
