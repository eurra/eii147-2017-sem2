
package examen;

public abstract class Encuestado {
    private String nombre;
    private String rut;
    private String seleccion;
    
    public Encuestado(String nom, String rut, String selec) { 
        this.nombre = nom;
        this.rut = rut;
        this.seleccion = selec;
    }

    public String getRut() {
        return rut;
    }
    
    public abstract double calcularMargenError(int votos);
}
