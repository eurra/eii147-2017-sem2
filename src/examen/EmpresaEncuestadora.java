
package examen;

public class EmpresaEncuestadora {
    private String nombreEmpresa;
    private String representante;
    private Encuesta [] encuestas; 
    private int pLibreEncuestas;
    
    public EmpresaEncuestadora(String nombreEmpresa, String representante, int tamEncuestas) { 
        this.nombreEmpresa = nombreEmpresa;
        this.representante = representante;
        this.encuestas = new Encuesta[tamEncuestas];
        this.pLibreEncuestas = 0;
    }
    
    public boolean agregarEncuestado(int numeroEncuesta, Encuestado nuevoEncuestado, int votos) {
        int posEncuesta = buscarEncuesta(numeroEncuesta);
        
        if(posEncuesta == -1)
            return false;
        
        return encuestas[posEncuesta].agregarEncuestado(nuevoEncuestado, votos);
    }
    
    public Encuestado[] eliminarEncuesta(int numeroEncuesta) {
        int posEncuesta = buscarEncuesta(numeroEncuesta);
        
        if(posEncuesta == -1)
            return null;
        
        Encuestado[] encs = encuestas[posEncuesta].obtenerEncuestadosOrdenados();
        encuestas[posEncuesta] = encuestas[pLibreEncuestas - 1];
        pLibreEncuestas--;
        
        return encs;
    }
    
    private int buscarEncuesta(int numEncuesta) {
        for(int i = 0; i < pLibreEncuestas; i++) {
            if(encuestas[i].getNumero() == numEncuesta)
                return i;
        }
        return -1;
    }
}
