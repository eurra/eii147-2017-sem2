
package sem17;

public class EspacioFisico {
    private String nombre;
    private int capacidad;
    private NodoRegistro head;

    public EspacioFisico(String nombre, int capacidad) {
        this.nombre = nombre;
        this.capacidad = capacidad;
        this.head = new NodoRegistro(null, null); // Nodo fantasma
    }

    public String getNombre() {
        return nombre;
    }

    public int getCapacidad() {
        return capacidad;
    }
    
    public boolean existeRegistro(int num) {
        NodoRegistro aux = head.getNext();
        
        while(aux != null) {
            if(aux.getDato().getNumero() == num)
                return true;
            
            aux = aux.getNext();
        }
        
        return false;
    }
    
    public int sumarDuracionActividades() {
        return sumarDuracionActividades(false);
    }
    
    public int sumarDuracionActividades(boolean soloCatedras) {
        NodoRegistro aux = head.getNext();
        int total = 0;
        
        while(aux != null) {
            if(!soloCatedras || aux.getDato() instanceof ClaseCatedra)
                total += aux.getDato().calcularTiempoDuracion();
            
            aux = aux.getNext();
        }
        
        return total;
    }
    
    public boolean agregarRegistroUso(RegistroUso reg) {
        if(reg.getCantidadAsistentes() > capacidad)
            return false;
        
        NodoRegistro nuevo = new NodoRegistro(reg, null);
        NodoRegistro aux = head;
        
        while(aux.getNext() != null)
            aux = aux.getNext();
        
        aux.setNext(nuevo);
        return true;
    }
    
    public Tutoria obtenerTutoriaMayorDuracion() {
        Tutoria mayor = null;
        int duracionMayor = 0;
        
        NodoRegistro aux = head.getNext();
        
        while(aux != null) {
            if(aux.getDato() instanceof Tutoria) {
                int duracionActual = aux.getDato().calcularTiempoDuracion();
                
                if(mayor == null || duracionMayor < duracionActual) {
                    mayor = (Tutoria)aux.getDato();
                    duracionMayor = duracionActual;
                }
            }
            
            aux = aux.getNext();
        }
        
        return mayor;
    }
    
    public boolean eliminarRegistroUso(int id) {
        if(head.getNext() == null)
            return false;
        
        NodoRegistro aux = head;
        
        while(aux.getNext() != null) {
            if(aux.getNext().getDato().getNumero() == id) {
                aux.setNext(aux.getNext().getNext());
                return true;
            }
            
            aux = aux.getNext();
        }
        
        return false;
    }
    
    /**
     * Realiza los siguientes pasos:
     * 1) Valida cantidad asistentes
     * 2) Reordena la lista, generando un head nuevo. Se toma cada elemento del head
     *    antiguo y se van agregando ordenados en el head nuevo, usando el método
     *    privado agregarOrdenadoPorNumero()
     * 3) Se reemplaza el head viejo por el nuevo.
     * 4) Se agrega el registro nuevo ordenado, usando el head nuevo.
     */
    public boolean agregarRegistroUsoOrdenado(RegistroUso reg) {
        if(reg.getCantidadAsistentes() > capacidad)
            return false;
        
        NodoRegistro headTemp = new NodoRegistro(null, null);
        reordenar(head, headTemp);
        head = headTemp;
        agregarOrdenadoPorNumero(head, reg);
        return true;
    }
    
    private void reordenar(NodoRegistro headViejo, NodoRegistro headNuevo) {
        NodoRegistro aux = headViejo.getNext();
        
        while(aux != null) {
            agregarOrdenadoPorNumero(headNuevo, aux.getDato());
            aux = aux.getNext();
        }
    }
    
    private void agregarOrdenadoPorNumero(NodoRegistro head, RegistroUso reg) {
        NodoRegistro aux = head;
        boolean agregado = false;
        
        while(!agregado && aux.getNext() != null) {
            if(aux.getNext().getDato().getNumero() > reg.getNumero()) {
                aux.setNext(new NodoRegistro(reg, aux.getNext()));
                agregado = true;
            }
            
            aux = aux.getNext();
        }
        
        if(!agregado)
            aux.setNext(new NodoRegistro(reg, null));
    }
}
