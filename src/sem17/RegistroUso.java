
package sem17;

public abstract class RegistroUso {
    private int numero;
    private String rutEncargado;
    private int cantidadAsistentes;
    private String fechaRealizacion;

    public RegistroUso(int numero, String rutEncargado, int cantidadAsistentes, String fechaRealizacion) {
        this.numero = numero;
        this.rutEncargado = rutEncargado;
        this.cantidadAsistentes = cantidadAsistentes;
        this.fechaRealizacion = fechaRealizacion;
    }

    public int getNumero() {
        return numero;
    }

    public String getRutEncargado() {
        return rutEncargado;
    }
    
    public int getCantidadAsistentes() {
        return cantidadAsistentes;
    }

    public String getFechaRealizacion() {
        return fechaRealizacion;
    }
    
    public abstract int calcularTiempoDuracion();
}
