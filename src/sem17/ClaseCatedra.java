
package sem17;

public class ClaseCatedra extends RegistroUso {
    private String siglaAsignatura;
    private int paralelo;
    private int cantidadBloques;

    public ClaseCatedra(int numero, String siglaAsignatura, int paralelo, int cantidadBloques, String rutEncargado, int cantidadAsistentes, String fechaRealizacion) {
        super(numero, rutEncargado, cantidadAsistentes, fechaRealizacion);
        this.siglaAsignatura = siglaAsignatura;
        this.paralelo = paralelo;
        this.cantidadBloques = cantidadBloques;
    }

    public String getSiglaAsignatura() {
        return siglaAsignatura;
    }

    public int getParalelo() {
        return paralelo;
    }

    public int getCantidadBloques() {
        return cantidadBloques;
    }
    
    @Override
    public int calcularTiempoDuracion() {
        return (int)(cantidadBloques * 1.5 * 60);
    }
}
