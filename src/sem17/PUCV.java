
package sem17;

public class PUCV {
    private String rector;
    private EspacioFisico[][] espacios;
    private String[] campus;
    private int pLibreCampus;
    
    public PUCV(String rector, int totalCampus, int espaciosPorCampus) {
        this.rector = rector;
        espacios = new EspacioFisico[espaciosPorCampus][totalCampus];
        campus = new String[totalCampus];
    }

    public String getRector() {
        return rector;
    }
    
    public boolean agregarCampus(String nombre) {
        if(pLibreCampus == campus.length)
            return false;
        
        int colCampus = buscarIndiceCampus(nombre);
        
        if(colCampus != -1)
            return false;
        
        campus[pLibreCampus] = nombre;
        pLibreCampus++;
        return true;
    }
    
    public boolean agregarEspacioFisico(String nombreCampus, String nombreEspacio, int capEspacio) {
        return agregarEspacioFisico(nombreCampus, new EspacioFisico(nombreEspacio, capEspacio));
    }
    
    public boolean agregarEspacioFisico(String nombreCampus, EspacioFisico espacio) {
        if(buscarPosEspacio(espacio.getNombre()) != null)
            return false;
        
        int colCampus = buscarIndiceCampus(nombreCampus);
        
        if(colCampus == -1)
            return false;
        
        for(int i = 0; i < espacios.length; i++) {
            if(espacios[i][colCampus] == null) {
                espacios[i][colCampus] = espacio;
                return true;
            }
        }
        
        return false;
    }
    
    public int eliminarEspacioFisico(String nombre) {
        int[] posEspacio = buscarPosEspacio(nombre);
        
        if(posEspacio == null)
            return -1;
        
        int totalDuracion = espacios[posEspacio[0]][posEspacio[1]].sumarDuracionActividades();
        espacios[posEspacio[0]][posEspacio[1]] = null;        
        return totalDuracion;
    }
    
    public boolean agregarRegistroUso(RegistroUso reg, String nombreEspacio) {
        return agregarRegistroUso(reg, nombreEspacio, false);
    }
    
    public boolean agregarRegistroUso(RegistroUso reg, String nombreEspacio, boolean ordenado) {
        for(int i = 0; i < pLibreCampus; i++) {
            for(int j = 0; j < espacios.length; i++) {
                if(espacios[i][j] != null && espacios[i][j].existeRegistro(reg.getNumero()))
                    return false;
            }
        }
        
        int[] posEspacio = buscarPosEspacio(nombreEspacio);
        
        if(posEspacio == null)
            return false;
        
        EspacioFisico esp = espacios[posEspacio[0]][posEspacio[1]];

        if(!ordenado)
            return esp.agregarRegistroUso(reg);
        else
            return esp.agregarRegistroUsoOrdenado(reg);
    }
    
    public String obtenerCarreraTutoriaMayorDuracion(String nombreCampus) {
        int colCampus = buscarIndiceCampus(nombreCampus);
        
        if(colCampus == -1)
            return null;
        
        Tutoria tutoriaMayor = null;
        int duracionTutoriaMayor = 0;
        
        for(int i = 0; i < espacios.length; i++) {
            if(espacios[i][colCampus] != null) {
                Tutoria tutoriaActual = espacios[i][colCampus].obtenerTutoriaMayorDuracion();
                
                if(tutoriaActual != null) {
                    int duracionTutoriaActual = tutoriaActual.calcularTiempoDuracion();
                    
                    if(tutoriaMayor == null || duracionTutoriaMayor < duracionTutoriaActual) {
                        tutoriaMayor = tutoriaActual;
                        duracionTutoriaMayor = duracionTutoriaActual;
                    }
                }
            }
        }
        
        if(tutoriaMayor != null)
            return tutoriaMayor.getCarrera();
        
        return null;
    }
    
    public boolean eliminarRegistroUso(int num) {
        for(int i = 0; i < pLibreCampus; i++) {                        
            for(int j = 0; j < espacios.length; j++) {
                if(espacios[j][i] != null && espacios[j][i].eliminarRegistroUso(num)) 
                    return true;
            }
        }
        
        return false;
    }
    
    public double calcularPromedioCatedrasPorCampus() {
        if(pLibreCampus == 0)
            return 0.0;
        
        double sumaTotal = 0.0;
        
        for(int i = 0; i < pLibreCampus; i++) {                        
            for(int j = 0; j < espacios.length; j++) {
                if(espacios[j][i] != null) 
                    sumaTotal += espacios[j][i].sumarDuracionActividades(true);
            }
        }
        
        return sumaTotal / pLibreCampus;
    }
    
    private int buscarIndiceCampus(String nombre) {
        for(int i = 0; i < pLibreCampus; i++) {
            if(campus[i].equals(nombre))
                return i;
        }
        
        return -1;
    }
    
    // Retorna un arreglo de largo 2, donde la pos. 0 es la fila y la pos. 1 es la columna
    private int[] buscarPosEspacio(String nombre) {
        for(int i = 0; i < pLibreCampus; i++) {
            for(int j = 0; j < espacios.length; j++) {
                if(espacios[j][i] != null && espacios[j][i].getNombre().equals(nombre))
                    return new int[] {j, i};
            }
        }
        
        return null;
    }
}
