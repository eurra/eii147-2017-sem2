
package sem17;

public class Tutoria extends RegistroUso {
    private String carrera;

    public Tutoria(int numero, String carrera, String rutEncargado, int cantidadAsistentes, String fechaRealizacion) {
        super(numero, rutEncargado, cantidadAsistentes, fechaRealizacion);
        this.carrera = carrera;
    }

    public String getCarrera() {
        return carrera;
    }
    
    @Override
    public int calcularTiempoDuracion() {
        return getCantidadAsistentes() * 20;
    }
}
