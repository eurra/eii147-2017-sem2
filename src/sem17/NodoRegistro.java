
package sem17;

public class NodoRegistro {
    private RegistroUso dato;
    private NodoRegistro next;

    public NodoRegistro(RegistroUso dato, NodoRegistro next) {
        this.dato = dato;
        this.next = next;
    }

    public RegistroUso getDato() {
        return dato;
    }

    public NodoRegistro getNext() {
        return next;
    }

    public void setNext(NodoRegistro next) {
        this.next = next;
    }
}
