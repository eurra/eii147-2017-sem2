
package prueba2;

public class Conductor {
    private String rut;
    private String nombre;
    private int horasContratadas;
    private int sueldo;

    public Conductor(String rut, String nombre, int horasContratadas, int sueldo) {
        this.rut = rut;
        this.nombre = nombre;
        this.horasContratadas = horasContratadas;
        this.sueldo = sueldo;
    }
    
    public Conductor(String rut, String nombre) {
        this(rut, nombre, 0, 0);
    }

    public String getRut() {
        return rut;
    }

    public String getNombre() {
        return nombre;
    }
}
