
package prueba2;

public class Programa {
    private String nombre;
    private String bloqueHorario;
    private double rating;
    private Conductor[] conductores;

    public Programa(String nombre, String bloqueHorario, double rating) {
        this.nombre = nombre;
        this.bloqueHorario = bloqueHorario;
        this.rating = rating;
        this.conductores = new Conductor[5];
    }

    public String getNombre() {
        return nombre;
    }
    
    public Conductor buscarConductor(String rut) {
        for(int i = 0; i < conductores.length; i++) {
            if(conductores[i] != null && conductores[i].getRut().equals(rut))
                return conductores[i];
        }
        
        return null;
    }
    
    public boolean agregarConductor(Conductor c) {
        if(buscarConductor(c.getRut()) != null)
            return false;
        
        for(int i = 0; i < conductores.length; i++) {
            if(conductores[i] == null) {
                conductores[i] = c;
                return true;
            }
        }
        
        return false;
    }
    
    public Conductor[] obtenerConductores() {
        int cant = 0;
        
        for(int i = 0; i < conductores.length; i++) {
            if(conductores[i] != null)
                cant++;
        }
        
        if(cant == 0)
            return null;
        
        Conductor[] res = new Conductor[cant];
        int j = 0;
        
        for(int i = 0; i < conductores.length; i++) {
            if(conductores[i] != null)
                res[j++] = conductores[i];
        }
        
        return res;
    }
}
