
package prueba2;

public class App {
    public static void main(String[] args) {
        TVCartoon tc = new TVCartoon("Jebus", 20);
        
        if(tc.agregarConductor("Pipiripao", "1234", "Roberto Nicolini"))
            System.out.println("Conductor agregado");
        else
            System.out.println("Conductor NO agregado");
        
        Conductor[] conds = tc.eliminarPrograma("Pipiripao");
        
        for(int i = 0; i < conds.length; i++)
            System.out.println("Conductor " + (i + 1) + ": " + conds[i].getNombre());
    }
}
