
package prueba2;

public class TVCartoon {
    private String gerente;
    private Programa[] programas;
    private int pLibre;

    public TVCartoon(String gerente, int cantProgramas) {
        this.gerente = gerente;
        
        if(cantProgramas < 20)
            this.programas = new Programa[20];
        else
            this.programas = new Programa[cantProgramas];
    }
    
    public boolean agregarConductor(String nombrePrograma, String rut, String nombreCond) {
        Conductor nuevo = new Conductor(rut, nombreCond);
        return agregarConductor(nombrePrograma, nuevo);
    }
    
    public boolean agregarConductor(String nombrePrograma, String rut, String nombreCond, int horasContratadas, int sueldo) {
        Conductor nuevo = new Conductor(rut, nombreCond, horasContratadas, sueldo);
        return agregarConductor(nombrePrograma, nuevo);
    }
    
    private boolean agregarConductor(String nombrePrograma, Conductor cond) {
        Conductor agregar = null;
        
        for(int i = 0; i < pLibre && agregar == null; i++) {
            agregar = programas[i].buscarConductor(cond.getRut());
        }
        
        if(agregar == null)
            agregar = cond;
        
        for(int i = 0; i < pLibre; i++)
        {
            if(programas[i].getNombre().equals(nombrePrograma))
                return programas[i].agregarConductor(agregar);
        }
        
        return false;
    }
    
    public Conductor[] eliminarPrograma(String nombre) {        
        for(int i = 0; i < pLibre; i++) {
            if(programas[i].getNombre().equals(nombre)) {
                Conductor[] res = programas[i].obtenerConductores();
                programas[i] = programas[pLibre - 1];
                pLibre--;
                
                return res;
            }
        }
        
        return null;
    }
}
